
from pymongo import MongoClient
import urllib.parse
import pickle 
import pandas as pd
from tslearn.utils import to_time_series_dataset
from tslearn.preprocessing import TimeSeriesScalerMeanVariance
from dtaidistance import dtw


class AppData():
    
    """Class to hold all of the cleint side data associated methods"""
    
    
    def __init__(self, path_conf):
        # connect to database 
        with open( path_conf) as f:
            creds = f.readlines()
        cred_dict = {k:v for k, v in zip( [i.replace("\n", "").split(" :")[0] for i in creds], 
                                         [i.replace("\n", "").split(": ")[-1] for i in creds], 
                                        )
                    }
        user_name = cred_dict['username']
        passw = cred_dict['password']
        uri = cred_dict['uri'].split(",")[0] 
        db_name = cred_dict['database']
        client = MongoClient(f'mongodb+srv://{user_name}:{urllib.parse.quote_plus(passw)}@{uri}/{db_name}') 
        
        print("Connection string ", client )
        
        self.db = client[db_name]
        return print("Connected to DB")
        
    
    def get_collection_data(self, collection, query = {}, subset ={}):
        """gets the data of a collection"""
        return self.db[collection].find( query, subset)
    
    def get_collection_as_dataframe(self, collection, query = {}, subset ={}):
        """gets the data of a collection"""
        
        data = self.get_collection_data(collection, query = query, subset = subset)
        df = pd.DataFrame(list(data)) 
        return df
    
    def get_ordered_df_from_collection(self, collection, sort_col = 'Datetime', query = {}, subset ={} ):
        """Gets a collection as an ordered collection"""
        
        df = self.get_collection_as_dataframe(collection =  collection, query = query, subset = subset )
        df = df.sort_values(sort_col)
        df.reset_index(inplace = True)
        
        try:
            df = df.drop('index', axis =1 ) # might fail here 
        except:
            print("No col 'index' to drop")
        
        return df
    
    def get_pickle_model(self, filepath):
        """Opens a pickle file from local file path"""
        
        with open(filepath, 'rb') as f:
            file = pickle.load(f)
            return file 
        
    def get_clusters_df(self, filepath):
        """From pickle serialised model get the dataFrame of clusters"""
        
        # get data and reshape 
        pickle_obj = self.get_pickle_model(filepath)
        data = pickle_obj['model_params']['cluster_centers_'].reshape( pickle_obj['model_params']['cluster_centers_'].shape[0], 
                                                     pickle_obj['model_params']['cluster_centers_'].shape [1], 
                                                                     )
        #make dataframe 
        df_clusters = pd.DataFrame(data = data).T

        # rename columns 
        colrename = [f"cluster_{i+1}" for i in list(df_clusters)]
        renamedict = {k : v for k, v in zip( list( df_clusters), colrename )}
        df_clusters.rename(renamedict, axis =1, inplace = True)

        return df_clusters
    
    def get_cluster_for_nodes(self, df_nodes, pickle_model): # needs altering to be "stateless"
        """Gets the cluster for each node in pciklefile dependiong on orginal dataframe """
        
        nodes_all = [i for i in list(df_nodes) if i not in ['Datetime', 'Precipitation', 'rain_shift-1',  'rain_6h_mean',  'rain_6h_sum'] ]
        dic_node_to_cluster = { k: v for k, v in zip(nodes_all, pickle_model['model_params']['labels_']+1)}
        return dic_node_to_cluster
    
    def get_cost_matrix_data(self, node1, node2, df, scaled_dataset):
        """For 2 nodes get the dtw data"""
        s1 =  scaled_dataset[df.columns.get_loc(node1)][-24*7*4: ]
        s2 =  scaled_dataset[df.columns.get_loc(node2)][-24*7*4: ]

        d, paths = dtw.warping_paths(s1, s2, #window=25, #psi=2
                                   )
        best_path = dtw.best_path(paths)

        return s1, s2, best_path


