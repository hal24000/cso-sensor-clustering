import streamlit as st
import streamlit.components.v1 as components
import pandas as pd

from tslearn.utils import to_time_series_dataset
from tslearn.preprocessing import TimeSeriesScalerMeanVariance
from collections import Counter



from st_aggrid import GridOptionsBuilder, AgGrid, GridUpdateMode, DataReturnMode, JsCode

from setup.streamlit_templates import   StreamLitTemplates, SideBars
from setup.clustering_data import AppData
from visualization.clustering_plots import ClusteringPlots


#################### PAGE SETUP & SIDEBAR ###################################

page = StreamLitTemplates(page_title = 'CSO Clustering') # define class 
page.setup_page() # setup page
page.hide_presets() # hide presets 

sidebar = SideBars() #define class
time_select, cluster_number, metric_select, default_cutoff, cutoff_select, graph_select, submit_button = sidebar.side_bar_form_clustering() # get side bar for clustering 

info_text = """
    ### :information_source: Info
    Dynamic Time Warping used for:\n
    - KMeans clustering 
    - Correlation Codefficents 
    \nFor high correlations, a case can be made to remove the least connected sensor.
    """

sidebar.info_general(info_text) # get info for clustering 

#################### END PAGE SETUP & SIDEBAR ###################################


################### Get Data #######################

appdata = AppData(path_conf = "conf/conf.txt")

dfreadings = appdata.get_ordered_df_from_collection(collection = 'WESSEX_E_Numbers_Apr_2019_60Min_Mean', subset = {'_id' : 0})
dfcorrelation = appdata.get_collection_as_dataframe(collection = 'kk_sensor_correlation', subset = {'_id' : 0})
dfloc = appdata.get_collection_as_dataframe(collection =  'cso_demo_table_asset_location', subset = {'_id' : 0} )
dfclusters = appdata.get_clusters_df(filepath = f'../../waterdemo-shared/clustering/models/kmeans_DTW_{cluster_number}clusters.pickle') # changefilename on select
dfclusters['Datetime'] = dfreadings['Datetime'] # at risk of being uncontrolled - need a check 


pickle_model = appdata.get_pickle_model(filepath = f'../../waterdemo-shared/clustering/models/kmeans_DTW_{cluster_number}clusters.pickle' )
#get_cluster_for_nodes
dic_node_to_cluster = appdata.get_cluster_for_nodes(df_nodes = dfreadings, pickle_model = pickle_model )

print("dic, ", dic_node_to_cluster)

dfcorrelation['Cluster Node 1'] = dfcorrelation['CSO 1'].map(dic_node_to_cluster)
dfcorrelation['Cluster Node 2'] = dfcorrelation['CSO 2'].map(dic_node_to_cluster)


dfloc[f'cluster'] = dfloc['node_id'].map(dic_node_to_cluster)
dfloc = dfloc.sort_values('cluster')
print(dfloc.isna().sum())
#dfloc[f'cluster'] = dfloc[f'cluster'].astype('int')
dfloc[f'cluster'] = dfloc[f'cluster'].astype('str')
dfloc[f'clusters_text'] = "cluster " + dfloc[f'cluster']


# for compare plot # to change 
dfall = pd.concat([dfreadings, dfclusters ], axis = 1)
dfall = dfall.loc[:,~dfall.columns.duplicated()] # drop duplicated datetime column 

# for DTW cost matrix
formatted_dataset = to_time_series_dataset([dfreadings[i].values for i in list(dfreadings)])
scaled_dataset = TimeSeriesScalerMeanVariance().fit_transform(formatted_dataset)


################ END GET DATA #########################

############### PAGE START ACTIONABLE INSIGHTS ############################

cluster_count = Counter(dic_node_to_cluster.values())
cluster_count = dict(sorted(cluster_count.items(), key=lambda item: item[1]))
cluster_count = [f"cluster {k} : {v} nodes ; " for k, v in cluster_count.items() ]
cluster_count.reverse()
#cluster_text = f"In each cluster there are the following values, listed from highest to lowest cluster: {''.join(cluster_count)}"

st.success(
    f""" When splitting CSOs into {cluster_number} clusters the clusters are split geographically as shown in the map below. \n For {cluster_number} clusters the breakdown of sensors by clusters is as follows, listed from highest to lowest cluster: {''.join(cluster_count)}.
     Click on the table rows below to compare sensors to each other"""
    )


HtmlFile = open(f"../../waterdemo-shared/clustering/maps/lightmap_{cluster_number}_clusters.html", 'r', encoding='utf-8')
source_code_map = HtmlFile.read() 

components.html(source_code_map, height = 350 )

c0_0 = st.beta_columns(1)
gb = GridOptionsBuilder.from_dataframe(dfcorrelation)
gb.configure_selection("single")
grid_response = AgGrid(
    dfcorrelation,
    gridOptions=gb.build(),
    height=305,
    width=200,
    update_mode=GridUpdateMode.SELECTION_CHANGED,
    fit_columns_on_grid_load=True,
)


selected = grid_response["selected_rows"]
if len(selected) > 0:
    selected = grid_response["selected_rows"]
else:
    selected = [
        {
            "CSO 1": "16067",
            "CSO 2": "16068",
            "Connect": 1,
            "Dist (km)": 0.22,
            "Pearson": 0.97,
            "Spearman": 0.91,
            "DTW": 1.72,
            "Min Connect": "16068",
            "Cluster Node 1": 0,
            "Cluster Node 2" : 0, 
       }
    ]

selected_1 = selected[0]["CSO 1"]
selected_2 = selected[0]["CSO 2"]
cso_select = [selected_1, selected_2]


############### END PAGE START ACTIONABLE INSIGHTS ############################

############### PLOTS ############################################

plots = ClusteringPlots()
clusters_plot = plots.plot_all_clusters(dfclusters = dfclusters,  CLUSTERS = cluster_number )
cluster_map = plots.plot_sensor_clusters_by_loc(selected_node_1 = selected_1, 
                                                selected_node_2 = selected_2, 
                                                dfloc = dfloc, 
                                                cluster_number = cluster_number, 
                                               )
compare_plot = plots.plot_clusters_of_pair(df  = dfall, nodes = [selected_1, selected_2],   
                                           dic_node_to_cluster = dic_node_to_cluster,  
                                           date_col = 'Datetime',)

# Note in Data class 
s1, s2, best_path = appdata.get_cost_matrix_data(node1 = selected_1, 
                                         node2 = selected_2, 
                                         df = dfreadings,
                                         scaled_dataset = scaled_dataset, 
                                        
                                        )

cost_matrix = plots.plot_cost_matrix(s1 = s1, s2 =s2, best_path = best_path, df = dfreadings, node1 = selected_1, 
                                         node2 = selected_2)


############### END OF PLOTS ############################################

############## PAGE MAIN CONTENTS #############################################




with st.beta_expander("View All Clusters" ):
    st.plotly_chart(clusters_plot, use_container_width=True)

with st.beta_expander("View Cluster Map" ):
    st.plotly_chart(cluster_map, use_container_width=True)
    
    
    
with st.beta_expander("Compare Sensors" ):
    st.plotly_chart(compare_plot, use_container_width=True)
    
with st.beta_expander("DTW Cost Matrix" ):
    st.plotly_chart(cost_matrix, use_container_width=True)




############## END OF PAGE MAIN CONTENTS #############################################